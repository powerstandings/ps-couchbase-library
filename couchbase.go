package couchbase

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strings"

	"github.com/golang/glog"
	"gopkg.in/couchbaselabs/gocb.v1"
)

var cluster = &gocb.Cluster{}
var Couchbase = &gocb.Bucket{}

type ICouch interface {
	GetConnection() *gocb.Bucket
	Init(
		nodes []string,
		username string,
		password string,
		bucket string,
		bucket_password string,
		docs_path string,
	)
	SyncViews(
		username string,
		password string,
		docs_path string,
	) error
}

type Couch struct {
	ICouch
}

type PSNDesignDocument struct {
	Name         string               `json:"name"`
	Views        map[string]gocb.View `json:"views,omitempty"`
	SpatialViews map[string]gocb.View `json:"spatial,omitempty"`
}

// GetCouch returns an instance of an ICouch interface implementation for use.
func GetCouch() *Couch {
	var couchbase = Couch{}
	return &couchbase
}

// GetConnection returns the current in memory Couchbase bucket connection.
func (c *Couch) GetConnection() *gocb.Bucket {
	return Couchbase
}

// Init makes a connection to a Couchbase cluster, returns a bucket connection,
// and stores the connection in memory for future use.
func (c *Couch) Init(
	nodes []string,
	username string,
	password string,
	bucket string,
	bucket_password string,
	docs_path string,
) (*gocb.Bucket, error) {

	var err error

	for _, host := range nodes {
		connection_string := "couchbase://" + host

		glog.Infof("Trying connection to couchbase at: %s\n", connection_string)

		cluster, err = gocb.Connect(connection_string)
		if err != nil {
			glog.Errorf("Not able to connect to Couchbase at %s because: %s. Trying another node...\n", host, err.Error())
			continue
		} else {
			glog.Infof("Opening couchbase bucket: %s with password: %s", bucket, bucket_password)
			Couchbase, err = cluster.OpenBucket(bucket, bucket_password)
			if err != nil {
				glog.Errorf("Not able to open Couchbase bucket at %s because: %s. Trying another node...\n", host, err.Error())
			} else {
				break
			}
		}
	}

	return Couchbase, err
}

// SyncViews runs migrations on your desing documents at the specified path.
func (c *Couch) SyncViews(
	username string,
	password string,
	docs_path string,
) error {
	manager := Couchbase.Manager(username, password)

	glog.Infof(
		"Connected to bucket manager successfully, looking for design documents in %s\n",
		docs_path,
	)
	files, err := ioutil.ReadDir(docs_path)
	if err != nil {
		return err
	}

	docs := []gocb.DesignDocument{}

	for _, file := range files {
		var doc PSNDesignDocument
		var gocb_doc gocb.DesignDocument
		if strings.HasSuffix(file.Name(), ".json") {
			jfile, err := ioutil.ReadFile(docs_path + "/" + file.Name())
			failOnError(err, "can't read design document from disk")
			glog.Infof("Loaded document file: %s\n", file.Name())
			err = json.Unmarshal(jfile, &doc)
			if err != nil {
				glog.Errorf(
					"Unable to unmarshal design document, check for valid format: %s\n",
					file.Name(),
				)
				continue
			}
			gocb_doc.Name = doc.Name
			gocb_doc.SpatialViews = doc.SpatialViews
			gocb_doc.Views = doc.Views
			docs = append(docs, gocb_doc)
		}
	}

	for _, doc := range docs {
		_, err := manager.GetDesignDocument(doc.Name)
		if err != nil {
			glog.Infof("No existing design document named: %s\n", doc.Name)
			err = manager.InsertDesignDocument(&doc)
			if err != nil {
				glog.Errorf("Unable to insert design doc: %v because: %s\n", doc, err)
				return err
			}
			glog.Infof("Successfully inserted new design document: %s\n", doc.Name)
		} else {
			glog.Info("Updating existing design document...")
			err = manager.UpsertDesignDocument(&doc)
			if err != nil {
				return err
			}
			glog.Infof("Successfully upserted existing design document: %s\n", doc.Name)
		}
	}

	return nil
}

// function to be called on fatal errors, this kills the app
func failOnError(err error, msg string) {
	if err != nil {
		glog.Fatalf("%s: %s", msg, err)
		panic(fmt.Sprintf("%s: %s", msg, err))
	}
}
